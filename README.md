# Projet d'éclairage sur batterie 12V avec ampoules LED :

* gestion électronique de la batterie 12V pour éviter une décharge complète (Arduino)
* suivi du niveau de la batterie via une led RGB type "Neopixel"
* utilisateur de la cave pévenu d'un niveau bas par clignotement de l'éclairage principale en plus du changement de couleur
* support d'ampoule imprimable (impression 3D) avec fixation magnétique (le plafonde de la cave en question est tenu par des poutrelles en acier)
* jusqu'à une dizaine d'ampoules LED 4 Watts

## Liste des composants électroniques et électriques
* 1 Microcontrôleur ATMEL ATtiny85 avec son support 
* 1 régulateur linéaire LM7805
* 1 LED RGB "Neopixel" 5mm
* Relais 'prêt à l'emploi' sur breakout board (exemple : http://www.microbot.it/product/135/Relay-Module.html )
* résistances 1/4W
    * 1x 1 komh
    * 1x 10 kohm
    * 1x 47 kohm
* Condensateur :
    * 2x 10µF 50V électrolytique
    * 1x 0.1µF céramique
* 1 plaque de prototypage pour souder le tout (exemple : perma-proto board hlaf-size d'Adafruit, en optimisant un peu, la quarter size devrait suffir)
* 1 bornier à souder 2 bornes (optionnel : c'est possible de souder directement les fils à la place du bornier)
* fils 22AWG de différentes couleurs (au moins noir, rouge et une troisième) pour l'électronique
* cable 2x0.75mm2 type cable hifi : plusieurs mètres (section 0.75mm2 minimum, ça dépend du nombres et de la puissante totales des spots)
* gaine thermorétractable de différentes sectin et couleur
* 1 barette de 3 dominos 4 mm2
* 1 interrupteur à bascule acceptant plusieurs Ampères en 12V (10A c'est pas mal)
* 1 batterie acide-plomb 12V (type batterie de moto) avec les cosses adaptées si besoin
* 1 boite plastique suffisamment grande pour acceuillir la batterie choisie et les composants. IMPORTANT : PAS de boîte étanche, la batterie produit des gaz qui doivent pouvoir s'évacuer !
* visserie M2.5 et M3 : vis, rondelles, écrous, entretoises... pour fixer l'électronique

### Outillage

Le grand classique de l'électronique:
* pince coupante
* pince à dénuder
* fer à souder et ses accessoires

On y rajoute de quoi percer quelques trous de 2.5 et 3mm dans le plastique de la boite et les petits tournevis pour la visserie et les dominos.

## Liste du matériel pour _un_ spot
* 1 support d'ampoule ( à imprimer en 3D, si possible en ABS ou autre matériaux résitant à la chaleur)
* 1 base de fixation magnétique (à imprimer en 3D, le matériaux importe peu tant que c'est assez solide)
* 1 douille GU5.3 précablé
* 1 ampoule LED 12V GU5.3 (max 4W !!!)
* 1 barrette de 2 domino 4 mm2
* 1 aimant au neodyme diamètre 20mm, h=3mm, percé et chanfreiné
* visserie (si possible inox pour la cave: meilleur sur le long terme en cas d'humidité)
    * 2 vis M2.5 10mm + écrous
    * 1 vis M2.5 8mm
    * 1 vis M3 15mm + écrou + rondelle
    * 1 vis M4 15mm à tête fraisée 
    * éventuellement une ou deux rondelle(s) diamètre intérieur 3.5 ou 4mm

A multiplier par le nombre de spots voulus.
    
## Montage

Montage de l'ensemble est aussi présenté en photo dans le répertoire : photo/mouting instructions
    
### Montage du spot

* préparer les douilles:
    * recouper les fil de la douille GU5.3 à 8cm environ
    * dénuder et étamer les 2 fils en hésitant pas à épaissir un peu le bout pour une meilleure prise dans le domino
* faire passer les deux fil de la douille dans les trous du support d'ampoule (trous plus centraux qui n'ont pas d'emprunte hexagonale à l'extérieur)
* visser la douille sur le support d'ampoule avec les 2 vis M2.5 10mm et les écrous (qui se logent dans les empruntes hexagonales). Ne pas serrer trop fort ! la céramique de la douille peut casser!
* monter l'ampoule pour vérifier que tout va bien, puis l'enlever le temps de finir le montage
* fixer le support d'ampoule sur la base de fixation magnétique avec la vis M3 15mm et son écrou. Attention à l'orientation des pièces! Les filsS'il y a un peu trop de jeu, ajouter une rondelle.
* faire passer les deux fils de la douille dans les trous de la base
* visser les deux fils dans le domino
* fixer le domino dans son logement avec la vis M2.5 8mm et en repoussant les fils de la douille vers l'extérieur
* passer l'aimant dans son logement. Si n'arrive pas tout à fait à raz de la base, ajouter une rondelle derrière avant de visser avec la vis  M4
* placer l'ampoule et c'est terminé!

### Montage final

Attention aux court-circuits !!! une batterie 12V peut fournir une grosse puissance (la mienne, petite 6Ah => 100A soit ~1200W). Il n'y a pas de risque d'électrocution mais risque de brûlures sévères! 
Attention !!! les batteries acide/plomb sont remplies d'acide sulfurique concentré : risque de brûlure chimique en cas de fuite de batterie.

* connecter l'ensemble des spot LED entre eux en parallèle en faisant bien attention à ne pas faire de court circuit (utiliser le marquage coloré sur un des brins pour s'en assurer). Les lampes GU5.3 ne sont pas polarisés, pas de crainte de ce côté là.
* connecter le premier spots au domino dans la boîte : normalement un fil avec la sortie du relais (dont le COM est connecté au (+) de la batterie via l'interrupteur général) et un fils avec le (-) de la batterie.
* vérifier que l'interrupteur est sur "OFF" (circuit ouvert)
* connecter la batterie (masse (-) en premier).
* fermer la boîte avec la batterie dedans
* allumer pour vérifier que tout va bien (lumière verte si la batterie est chargée + lumière allumée)
